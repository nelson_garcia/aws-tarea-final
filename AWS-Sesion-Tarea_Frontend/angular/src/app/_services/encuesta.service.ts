import { Injectable } from '@angular/core';
import { EncuestasUsrs } from '../_model/EncuestasUsrs';
import { HttpClient } from '@angular/common/http';
import { HOST_BACKEND, TOKEN_NAME } from '../_shared/constants';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EncuestaService {

  urlEncuesta: string = `${HOST_BACKEND}/api/encuestas`;

  encuestaCambio = new Subject<EncuestasUsrs[]>();
  mensajeRegistro = new Subject<string>();

  constructor(private httpClient: HttpClient) { }

  listar(page: number, size: number) {
    return this.httpClient.get<EncuestasUsrs[]>(`${this.urlEncuesta}/listar?page=${page}&size=${size}`);
  }

  listarPorId(id: number) {
    return this.httpClient.get<EncuestasUsrs>(`${this.urlEncuesta}/${id}`);
  }

  registrar(encuesta: EncuestasUsrs) {
    return this.httpClient.post(`${this.urlEncuesta}/registrar`, encuesta);
  }

  modificar(encuesta: EncuestasUsrs) {
    return this.httpClient.put(`${this.urlEncuesta}/modificar`, encuesta);
  }

  eliminar(id: number) {
    return this.httpClient.delete(`${this.urlEncuesta}/eliminar/${id}`);
  }
}
