import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { EncuestasUsrs } from '../../../../_model/EncuestasUsrs';
import { EncuestaService } from '../../../../_services/encuesta.service';

@Component({
  selector: 'app-admin-edicion',
  templateUrl: './admin-edicion.component.html',
  styleUrls: ['./admin-edicion.component.css']
})
export class AdminEdicionComponent implements OnInit {

  eleccionUsr: string = 'Lenguaje JAVA';
  tipoLenguajes = [
    'Lenguaje JAVA',
    'Lenguaje C#',
  ];

  form: FormGroup;
  id: number;
  edicion: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private encuestaService: EncuestaService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombres': new FormControl('', Validators.required),
      'apellidos': new FormControl('', Validators.required),
      'edad': new FormControl(0),
      'eleccion': new FormControl(this.eleccionUsr),
      'profesion': new FormControl(''),
      'lugarDeTrabajo': new FormControl(''),
      'idUsuario': new FormControl('test')
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });

  }

  get f() { return this.form.controls; }

  initForm() {
    //EDITAR, por lo tanto carga la data a editar
    if (this.edicion) {
      this.encuestaService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'nombres': new FormControl(data.nombres),
          'apellidos': new FormControl(data.apellidos),
          'edad': new FormControl(data.edad),
          'eleccion': new FormControl(data.eleccion),
          'profesion': new FormControl(data.profesion),
          'lugarDeTrabajo': new FormControl(data.lugarDeTrabajo),
          'idUsuario': new FormControl(data.idUsuario)
        });
      });
    }
  }

  operar() {
    if(this.form.invalid) { return; }

    let row = new EncuestasUsrs();
    row.id = this.form.value['id'];
    row.nombres = this.form.value['nombres'];
    row.apellidos = this.form.value['apellidos'];
    row.edad = this.form.value['edad'];
    row.eleccion = this.form.value['eleccion'];
    row.profesion= this.form.value['profesion'];
    row.lugarDeTrabajo = this.form.value['lugarDeTrabajo'];
    row.idUsuario = this.form.value['idUsuario'];

    if (this.edicion) {
      //MODIFICAR
      this.encuestaService.modificar(row).subscribe(() => {
        this.encuestaService.listar(0,10).subscribe(data => {
          this.encuestaService.encuestaCambio.next(data);
          this.encuestaService.mensajeRegistro.next('SE MODIFICO');
        });
      });
    } else {
      //INSERTAR
      this.encuestaService.registrar(row).subscribe(() => {
        this.encuestaService.listar(0,10).subscribe(data => {
          this.encuestaService.encuestaCambio.next(data);
          this.encuestaService.mensajeRegistro.next('SE REGISTRO');
        });
      });
    }
    this.router.navigate(['/app/admin']);
  }

}
