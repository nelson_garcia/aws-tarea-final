import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSnackBar, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { EncuestasUsrs } from 'src/app/_model/EncuestasUsrs';
import { EncuestaService } from 'src/app/_services/encuesta.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  cantidad: number = 0;
  displayedColumns = ['id', 'nombres', 'apellidos', 'edad', 'profesion', 'lugarDeTrabajo', 'eleccion', 'acciones'];
  dataSource: MatTableDataSource<EncuestasUsrs>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    public route: ActivatedRoute,
    private encuestaService: EncuestaService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    //Solo cuando se hace next()
    this.cargarTabla(0, 100, false);

    this.encuestaService.mensajeRegistro.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
      this.cargarTabla(0, 100, false);
    })

  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  eliminar(id: number) {
    this.encuestaService.eliminar(id).subscribe(() => {
      this.encuestaService.listar(0,10).subscribe(data => {
        this.encuestaService.encuestaCambio.next(data);
        this.encuestaService.mensajeRegistro.next('SE ELIMINÓ');
      });
    });
  }

  mostrarMas(e: any) {
    this.cargarTabla(e.pageIndex, e.pageSize, true);
  }

  cargarTabla(pageIndex: number, pageSize: number, desdePaginador: boolean){
    this.encuestaService.listar(pageIndex, pageSize).subscribe((datos) => {
      let registros = JSON.parse(JSON.stringify(datos)).content;
      this.dataSource = new MatTableDataSource<EncuestasUsrs>(registros);
      this.cantidad = JSON.parse(JSON.stringify(datos)).totalElements;
      if(!desdePaginador){
        this.dataSource.paginator = this.paginator;
      }
    });
  }
}
