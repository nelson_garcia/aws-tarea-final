export class EncuestasUsrs {
    id: number;
    nombres: string;
    apellidos: string;
    edad: number;
    eleccion: string; 
    profesion: string;
    lugarDeTrabajo: string;
    idUsuario: string;
}