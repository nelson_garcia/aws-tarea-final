import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecurityComponent } from './pages/security/security.component';
import { GuardService } from './_services/guard.service';
import { LogoutComponent } from './pages/logout/logout.component';
import { LoginComponent } from './pages/login/login.component';
import { EncuestaComponent } from './pages/encuesta/encuesta.component';
import { AdminComponent } from './pages/admin/admin/admin.component';
import { AdminEdicionComponent } from './pages/admin/admin/edicion/admin-edicion.component';
import { BodyComponent } from './pages/body/body.component';
import { IndexComponent } from './pages/index/index.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [GuardService]},
  {path: 'logout', component: LogoutComponent},
  {path: 'security', component: SecurityComponent},
  
  {path: 'app', component: BodyComponent, children: [
    {path: 'index', component: IndexComponent},
    {path: 'admin', component: AdminComponent, children: [
        { path: 'nuevo', component: AdminEdicionComponent },
        { path: 'edicion/:id', component: AdminEdicionComponent }
    ]},
    {path: 'encuesta', component: EncuestaComponent},
  ], canActivate: [GuardService]},

  
  
  {path: '**', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
