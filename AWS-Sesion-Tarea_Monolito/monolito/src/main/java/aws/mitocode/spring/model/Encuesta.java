package aws.mitocode.spring.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "encuestas")
public class Encuesta implements Serializable {

	/**
	 * Serial de clase.
	 */
	private static final long serialVersionUID = 9028752387233495693L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "nombres", nullable = false)
	private String nombres;

	@Column(name = "apellidos", nullable = false)
	private String apellidos;
	
	@Column(name = "edad", nullable = false)
	private Integer edad;
	
	@Column(name = "eleccion", nullable = false)
	private String eleccion;

	@Column(name = "profesion", nullable = true)
	private String profesion;
	
	@Column(name = "lugar_de_trabajo", nullable = true)
	private String lugarDeTrabajo;
	
	@Column(name = "usuario", nullable = false)
	private String idUsuario;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	public String getEleccion() {
		return eleccion;
	}

	public void setEleccion(String eleccion) {
		this.eleccion = eleccion;
	}
	
		/**
	 * @return the profesion
	 */
	public String getProfesion() {
		return profesion;
	}

	/**
	 * @param profesion the profesion to set
	 */
	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	/**
	 * @return the lugarDeTrabajo
	 */
	public String getLugarDeTrabajo() {
		return lugarDeTrabajo;
	}

	/**
	 * @param lugarDeTrabajo the lugarDeTrabajo to set
	 */
	public void setLugarDeTrabajo(String lugarDeTrabajo) {
		this.lugarDeTrabajo = lugarDeTrabajo;
	}
	
	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

}
