package aws.mitocode.spring.service;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;

import aws.mitocode.spring.model.Encuesta;

public interface IEncuestaService {

	Page<Encuesta> listar(Pageable pageable, String usuario, Collection<GrantedAuthority> ltaRoles);
	Encuesta listarPorId(int id);
	void registrar(Encuesta encuesta);
	void modificar(Encuesta encuesta);
	void eliminar(int id);
}
