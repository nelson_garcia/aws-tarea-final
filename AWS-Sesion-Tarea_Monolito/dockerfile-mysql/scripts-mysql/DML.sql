USE tareabd;

INSERT INTO encuestas VALUES(1, "Joe", "Cusac", 23, 'Lenguaje JAVA', "Consultor", "Silicon valley", "admin");
INSERT INTO encuestas VALUES(2, "Angelina", "Pit", 32, 'Lenguaje C#', "Asesor", "Cojute", "admin");
INSERT INTO encuestas VALUES(3, "Jenifer", "Anniston", 55,  'Lenguaje JAVA', "Vendedor", "Los angeles","admin");
INSERT INTO encuestas VALUES(4, "Luie", "Ferrignio", 27, 'Lenguaje C#', "Comprador", "China","admin");
INSERT INTO encuestas VALUES(5, "Arnold", "Shazeneger", 44, 'Lenguaje C#', "Ingeniero", "Austria","admin");

COMMIT;