#!bin/bash
echo "################## Construyendo imagen docker ##################"
docker build -t api-encuesta .
echo "################## Tag de la imagen docker ##################"
docker tag api-encuesta:latest 741954040391.dkr.ecr.us-east-1.amazonaws.com/api-encuesta:latest
echo "################## Subiendo a AWS ECS repository ##################"
$(aws ecr get-login --no-include-email --region us-east-1)
docker push 741954040391.dkr.ecr.us-east-1.amazonaws.com/api-encuesta:latest
echo "################## Forzando la actualizacion SERVICE ECS ##################"
aws ecs update-service --cluster CursoAWS --service srvEncuesta --force-new-deployment
