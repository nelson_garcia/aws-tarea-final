package aws.mitocode.spring.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import aws.mitocode.spring.dao.IEncuestasUsrsDao;
import aws.mitocode.spring.model.EncuestasUsrs;
import aws.mitocode.spring.service.IEncuestasUsrsService;


@Service
public class EncuestasUsrsServiceImpl implements IEncuestasUsrsService {
	
	@Autowired
	private IEncuestasUsrsDao encuestaDao;
	
	
	@Override
	public Page<EncuestasUsrs> listar(Pageable pageable, String usuario, Collection<GrantedAuthority> ltaRoles) {
		boolean isAdmin = false;
		if(ltaRoles != null && ltaRoles.size() > 0) {
			for(GrantedAuthority rol : ltaRoles) {
				if("ROLE_ADMIN".equalsIgnoreCase(rol.getAuthority())) {
					isAdmin = true;
					break;
				}
			}
		}
		if(isAdmin) {
			return encuestaDao.obtenerEncuestas(pageable);
		}
		return encuestaDao.obtenerEncuestasPorUsuario(pageable, usuario);
	}

	@Override
	public EncuestasUsrs listarPorId(int id) {
		return encuestaDao.findOne(id);	
	}

	@Override
	public void registrar(EncuestasUsrs encuesta) {
		encuestaDao.save(encuesta);
	}

	@Override
	public void modificar(EncuestasUsrs encuesta) {
		encuestaDao.save(encuesta);
	}
	
	@Override
	public void eliminar(int id) {
		encuestaDao.delete(id);		
	}

}
