package aws.mitocode.spring.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import aws.mitocode.spring.model.EncuestasUsrs;

@Repository
public interface IEncuestasUsrsDao extends JpaRepository<EncuestasUsrs, Integer> {

	@Query(value = "select f"
			+ " from EncuestasUsrs f"
			+ "	where f.idUsuario=:usuario",
			countQuery = "select count(f)"
					+ " from EncuestasUsrs f"
					+ "	where f.idUsuario=:usuario")
	Page<EncuestasUsrs> obtenerEncuestasPorUsuario(Pageable pageable, @Param("usuario") String usuario);
	
	@Query(value = "select f from EncuestasUsrs f",
			countQuery = "select count(f) from EncuestasUsrs f")
	Page<EncuestasUsrs> obtenerEncuestas(Pageable pageable);
}
