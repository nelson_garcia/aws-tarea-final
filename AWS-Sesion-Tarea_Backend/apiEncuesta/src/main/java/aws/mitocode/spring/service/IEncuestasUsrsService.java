package aws.mitocode.spring.service;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;

import aws.mitocode.spring.model.EncuestasUsrs;

public interface IEncuestasUsrsService {

	Page<EncuestasUsrs> listar(Pageable pageable, String usuario, Collection<GrantedAuthority> ltaRoles);
	EncuestasUsrs listarPorId(int id);
	void registrar(EncuestasUsrs encuesta);
	void modificar(EncuestasUsrs encuesta);
	void eliminar(int id);
}
