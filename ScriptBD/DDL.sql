CREATE DATABASE IF NOT EXISTS tareabd CHARACTER SET latin1 COLLATE latin1_swedish_ci;

USE tareabd;

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS encuestas;

SET foreign_key_checks = 1;


CREATE TABLE encuestas (
   	id INT(5) NOT NULL AUTO_INCREMENT,
    nombres VARCHAR(50) NOT NULL,
	apellidos VARCHAR(50) NOT NULL,
    edad INT(2) NOT NULL,
    eleccion VARCHAR(50) NOT NULL,
    profesion VARCHAR(150),
    lugar_de_trabajo VARCHAR(150),
    usuario VARCHAR(50) NOT NULL,
    
    
    INDEX (id),
    PRIMARY KEY (`id`)
) ENGINE=INNODB;
/*12Ma05yo12052020*/